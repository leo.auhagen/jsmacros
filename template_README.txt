# jsMacros

!Be advised that you are using any of this at your own risk!

!Code added into the HTML or markdown file might be executed maliciously!

A few simple scripts to enhance the experience in the internet:

### How to add own stuff:

1. create a new feature branch

2. add a file "yourfeature.js"

3. add feature info to the ```inputFiles``` array in "generate.js" according to this format

```{title: "name", description: "a meaningful description", file: "./filename.js"}```

4. run ```node generate.js```

6. test it you can easily access the Bookmarklets here: 
````
file:///path-to-repo/macros.html
````

7. commit and push the changes

8. create a merge request

### keep in mind while creating new bookmarklets

1. ```// comment``` will be replaced with ```/*comment*/```. Keep this in mind while using urls like "http://"

# The Bookmarklets