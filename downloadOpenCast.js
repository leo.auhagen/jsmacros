function getVideoCustomId(callback) {
    var iframesrc = document.getElementsByTagName("iframe")[0].src;
    if(iframesrc.substring(iframesrc.length - 42, iframesrc.length - 36) == "/play/")
        return callback(`https:/` + `/engage.streaming.rwth-aachen.de/search/episode.json?id=${iframesrc.substring(iframesrc.length - 36)}`);
    var req = new XMLHttpRequest();
    req.onload = function () {
        var str = this.responseText;
        var url = `https:/` + `/engage.streaming.rwth-aachen.de/search/episode.json?id=${str.substring(str.indexOf("custom_id")+18, str.indexOf("custom_id")+54)}`;
        //console.log(url);
        return callback(url);
    };
    req.open("get", iframesrc, true);
    req.send();
}

function getVideoDirectUrl(jsonUrl) {
     var req = new XMLHttpRequest();
    req.onload = function () {
        //console.log(this.responseText);
        var result = this.responseText;
        var parsed = JSON.parse(result.replace("search-results","searchresults"));
        var tracks = parsed.searchresults.result.mediapackage.media.track;
        var popuptext = `<h1>${parsed.searchresults.result.mediapackage.title}</h1><br>`;
        for(let i = 0; i < tracks.length; i++) {
            if (tracks[i].mimetype == "video/mp4") {
                if (Math.floor(Math.random() * 20) == 10) { // rick roll with 1/20 chance
                    popuptext += `<a href="https:/` + `/www.youtube.com/watch?v=dQw4w9WgXcQ">${tracks[i].tags.tag[0]}</a><br>`;
                }
                popuptext += `<a href="${tracks[i].url}" download>${tracks[i].tags.tag[0]}</a><br>`;
            }
        }
        

        newPopupAlert(popuptext);

    };
    req.open("get", jsonUrl, true);
    req.withCredentials = true;
    req.send();
}
var div = document.createElement("div");
div.classList.add("popupspace");
document.body.appendChild(div);
div.style = `position: fixed;
    right: 0;
    bottom: 0;
    width: 0px;
    height: 0px;`;
getVideoCustomId((jsonUrl) => {getVideoDirectUrl(jsonUrl)});

function newPopupAlert(text) {
    document.getElementsByClassName("popupspace")[0].innerHTML = `
    <div id="blurbackground" style="
        background-color: rgba(101, 194, 248, 0.527);
        display: flex;
        justify-content: center;
        flex-wrap: nowrap;
        flex-direction: row;
        position: fixed;
        left: 0;
        top: 0;
        z-index: 6;
        height: 100%;
        width: 100%;
        ">
        <div id="popupflexclm" style="
            display: flex;
            justify-content: center;
            flex-direction: column;
            flex-wrap: nowrap;
            height: 100%;
            width: 300px;"
        >
            <div id='popup' 
                style="width: 300px;
                height: fit-content;
                border: 2px solid lightgray;
                text-align: center;
                background-color: white;
                color: black;
                padding-bottom: 10px;
                border-radius: 6px;"
            >
                    <p>${text}</p>
                    <br>
                    <button onclick="document.getElementsByClassName('popupspace')[0].innerHTML = '';" id="closebtn" 
                        style="
                        background-color: #1a73e8;
                        color: white;
                        border: none;
                        border-radius: 3px;
                        padding: 5px;"
                    >
                    CLOSE
                    </button>
                </div>
            </div>
    </div>`;
}

