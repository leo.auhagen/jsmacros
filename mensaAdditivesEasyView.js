var menu_site_aca = "https:/"+"/www.studierendenwerk-aachen.de/speiseplaene/academica-w.html";
var menu_site_iz = "https:/"+"/www.studierendenwerk-aachen.de/speiseplaene/ahornstrasse-w.html";
if(!(window.location.href == menu_site_aca || window.location.href == menu_site_iz)) {
    window.location.href = menu_site_aca;
}

var h2 = document.getElementsByTagName("h2")[0];
if (window.location.href == menu_site_aca) {
    h2.innerHTML += "<a href='" + menu_site_iz + "'> hier zur IZ Mensa</a>"
} else {
    h2.innerHTML += "<a href='" + menu_site_aca + "'> hier zur ACA Mensa</a>"
}

getAdditivesList = function() {

    divs = document.getElementsByTagName("div");
    console.log(divs.length + " divs found");

    var additives = null;
    for (var i = 0; i < divs.length; i++) {
        if(divs[i].id == "additives") {
            additives = divs[i].children[0].innerHTML;
            console.log("additives found");
        }
    }

    additives = additives.split(",").join("");
    additives = additives.substring(4);

    additives_words = additives.split(" ");

    return additives_words;
};

var additives_words = getAdditivesList();
var additives_dict = {};

var key = null;
var current_additive = "";

for (var i = 0; i < additives_words.length; i++) {
    var word = additives_words[i];
    if(word[0] == "(" && word[word.length - 1] == ")") {
        // it is a key
        if (key != null) {
            additives_dict[key] = current_additive;
            current_additive = "";
        }
        key = word.substring(1, word.length - 1)
    } else {
        //it is an additive
        if (current_additive != "") {
            current_additive += " ";
        }
        current_additive += word;
    }
}
additives_dict[key] = current_additive;

var additives_fields = document.getElementsByTagName("sup");
for (var j = 0; j < additives_fields.length; j++) {

    var additives_in_field = additives_fields[j].innerHTML.split(" ").join("").split(",");
    additives_fields[j].innerHTML = "";

    for (var i = 0; i < additives_in_field.length; i++) {
        if (i > 0) {
            additives_fields[j].innerHTML += ", ";
        }
        additives_fields[j].innerHTML += additives_dict[additives_in_field[i]];
    }    
}