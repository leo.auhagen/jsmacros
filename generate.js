const fs = require("fs");
const marked = require("marked");


const output = [];
const input = [
  {
    title: "Hello World - Bookmarklet",
    file: "./helloWorld.js",
    description: "it will greet the world"
  },
  {
    title: "moa speed",
    file: "./moa-youtube-speed.js",
    description: "provides options to increase video playbackRates -  be advised this is as much beta as it gets"
  },
  {
  	title: "Download Open Cast",
  	file: "./downloadOpenCast.js",
  	description: "Will download an OpenCast video if possible."
  },
  {
     title: "strava ad remover v1.0",
     description: "For non premium users there are annoying ads on the analysis page. This bookmarklet will simply remove that ad.",
     file: "./strava-ad-remover.js"
  },
  {
     title: "RWTH Mensa - Additives Easy View",
     description: "This will show the relevant additives in the Menu instead of down below.",
     file: "./mensaAdditivesEasyView.js"
  }
];



// replaces any characters with their hexadecimal code
function cleanUrl(url) {
  var spl = url.split("\n").join("").split("").map(c => {return "%"+c.charCodeAt(0).toString(16);}).join("");
  
  return spl;
}

console.log(fs.readFileSync(input[0].file).toString().split("\r").length + " Zeilen");

// convert each js file in `inputFiles` into a javascript URL
for (let i = 0; i < input.length; i++) {
  output.push(
    `javascript:(function(){${cleanUrl(fs.readFileSync(input[i].file).toString().split("\r").join("").split("\n").map(handleComments).join(" "))}})()`
  );
}

// remove all comments with "//" and replace them with "/**/" comments
function handleComments(str) {
  if (!str.includes("//")) return str;
    return str.substring(0, str.indexOf("//")) + "/*" + str.substring(str.indexOf("//") + 2) + "*/";
}

// read the README template
const template = fs.readFileSync("template_README.txt").toString();

var outputstring = "";

for (let i = 0; i < output.length; i++) {
  outputstring += `\n\n## [${input[i].title}](${output[i]})\n\n` + "````\n" + output[i] + "\n````\n\n" + input[i].description;
}

const mdstring = `${template}\n${outputstring}`;
// insert the generated code to the README
fs.writeFileSync("README.md", mdstring);
console.log("md file: done");
// insert the code as html to the macros.html
fs.writeFileSync("macros.html", marked.parse(mdstring));
console.log("html file: done");
