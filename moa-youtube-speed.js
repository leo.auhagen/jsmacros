var max_fromcoolbrowsermacros = null;

function runGOexecute1231313() {
    max_fromcoolbrowsermacros = window.prompt(`what speed do you want? (only numeric values)`, 2.5);
    console.log(`output: "${max_fromcoolbrowsermacros}"`);
    if (max_fromcoolbrowsermacros == "exit" || max_fromcoolbrowsermacros == "" || max_fromcoolbrowsermacros == null) return;
    while (isNaN(parseInt(max_fromcoolbrowsermacros)) || max_fromcoolbrowsermacros == 0) {
        alert(`unable to set speed to  ${max_fromcoolbrowsermacros}, non-numeric value`);
        max_fromcoolbrowsermacros = window.prompt(`what speed do you want? (only numeric values as multiples of standard speed)`, 3);
    }
    var steps_fromcoolbrowsermacros = false;
    if (window.confirm(`Do you want slow steps?`)) {
        steps_fromcoolbrowsermacros = 0.1;
    }

    inc_fromcoolbrowsermacros(steps_fromcoolbrowsermacros, max_fromcoolbrowsermacros);
}

function inc_fromcoolbrowsermacros(steps_fromcoolbrowsermacros, max_fromcoolbrowsermacros) {
    if (document.getElementsByTagName(`video`).length <= 0) {
        alert("no video element found");
        return;
    }

    if (steps_fromcoolbrowsermacros != false) {
        addVideosSpeed(steps_fromcoolbrowsermacros);
        console.log(`speed is now: ${document.getElementsByTagName('video')[0].playbackRate}`);
        if (document.getElementsByTagName(`video`)[0].playbackRate > max_fromcoolbrowsermacros) {
            setVideosSpeed(max_fromcoolbrowsermacros);
            speedLoopFinished_fromcoolbrowsermacros = true;
        } else {
            setTimeout(() => { inc_fromcoolbrowsermacros(steps_fromcoolbrowsermacros, max_fromcoolbrowsermacros); }, 6000);
        }
    } else {
        setVideosSpeed(max_fromcoolbrowsermacros);
    }
}

function setVideosSpeed(speed) {
    const vids = document.getElementsByTagName(`video`);
    for (let i = 0; i < vids.length; i++) {
        vids[i].playbackRate = speed;
    }
}

function addVideosSpeed(increment) {
    const vids = document.getElementsByTagName(`video`);
    for (let i = 0; i < vids.length; i++) {
        vids[i].playbackRate += increment;
    }
}

runGOexecute1231313()